class CellsLoginDm extends Polymer.Element {

  static get is() {
    return 'cells-login-dm';
  }

  static get properties() {
    return {
      body:{
        type:String
      }
    };
  }
  _validateLogin(login){
    console.log("_validateLogin");
    console.log(login);
    if(login!=null){
      this.body=this.formatBody(login);
      this.$.loginDP.generateRequest();
    }
  }

  formatBody(login){
    console.log('CellsLoginDm.generateRequest',login);

    let bo={};
    if(login.success!=null){
      login.success.detail
      bo={
        dni:login.success.detail.dni ,//event.userId,
        clave: login.success.detail.clave
      }
    }else{
      bo={
        dni:login.userId ,//event.userId,
        clave: login.password
      } 
    }
    return bo;
  }

  _handleLoginSuccess(evt){
    if(evt.detail!=null){
      var usuJson=JSON.stringify(evt.detail);

      sessionStorage.setItem('respuesta',usuJson);
      localStorage.setItem('respuesta',usuJson);
  
      this.dispatchEvent(new CustomEvent("succes_event",{
        bubbles: true,
        composed: true,
        detail: {'success': evt}
      }));
    }else{
      console.log("credenciales invalidas");
    }
  }
}

customElements.define(CellsLoginDm.is, CellsLoginDm);
